# Apache Spark and DataFrames #

After amazing 4 days [Apache Spark training](https://hortonworks.com/services/training/class/hdp-developer-enterprise-spark/) from HortonWorks I attended last week lets dive again into Big Data and Apache Spark.

## DataFrames and Apache Spark ##

DataFrames are distributed datasets organized into named columns which allow us to make SQL like queries to get the results from Big Data saved in many datasources like [Hive](https://hive.apache.org/), [Hadoop](http://hadoop.apache.org/), [HBase](https://hbase.apache.org/).... As our trainer told us at the HortonWorks training, we should always try to transform Big Data into DataFrames and then perform the requested operations, because writing the query operations by RDD transformations and actions manually is unefficient, which makes sense. So use RDD API only to get the DataFrames in the end, it saves time.

## Two approaches howto create Dataframe ##

* You know the structure of Big Data. DataFrames schema is created by reflection of the input RowType.
* You don't know the structure the data. Spark API is used to build DataFrame schema.

### (Scala) Create DataFrame by known structure ###

Scala Spark API supports creation of DataFrame from Scala [case classes](http://docs.scala-lang.org/tutorials/tour/case-classes.html).  
So let's test it. 

Prerequisities:

* Installed HortonWorks Sandbox, installing it into [virtualbox](http://hortonworks.com/wp-content/uploads/2013/03/InstallingHortonworksSandboxonWindowsusingVB.pdf) is super easy.
* Apache Spark skills.

### Input data for dataframes... ###

Lets prepare following **students.txt** file with structure **name,university,age**.

```
John,MIT,26
Isabella,Yale,32
Leonard,Caltech,45
Berry,Princeton,34
Howard,MIT,59
```

and lets put it into Hadoop HDFS filesystem:

```
-- connect to hortonworks sandbox
ssh root@localhost -p 2222
-- create spark directory in the HDFS
hdfs dfs -mkdir /spark
-- create mentioned file students.txt and put it into new HDFS directory
hdfs dfs -put students.txt /spark/students.txt
```

### Scala code to make dataframe from HDFS file... ###

put following code into **load_students.scala** file:

```
// lets prepare case class Student
case class Student(name:String,university:String,age:Int)

// lets create RDD from students.txt file
val rddFile = sc.textFile("/spark/students.txt")

// lets parse the input file line by line and get RDD [[lineArray1 words],[lineArray1 words]..]
val linesRDD = rddFile.map(line=>line.split(","))

// now lets create DataFrame by applying the case class Student onto every [lineX words array]
val studentsDF = linesRDD.map(lineArray=>Student(lineArray(0),lineArray(1),lineArray(2).toInt)).toDF()

// now register the students table with the students dataframe. In the last version of Spark use createOrReplaceTempView
studentsDF.registerTempTable("students")

// now lets select every MIT student in the Big Data storage:
val mitStudents = sqlContext.sql("SELECT name, university, age FROM students WHERE university = 'MIT'")

// show it
mitStudents.show()

// exit the shell
System.exit(0)
```

now run the scala file **against spark-shell**:

```
[root@sandbox ~]# spark-shell -i load_students.scala
```

if you prepared everything correctly then you should see in the output:


```
17/06/25 11:17:44 INFO DAGScheduler: Job 1 finished: show at <console>:28, took 0.047832 s
+------+----------+---+
|  name|university|age|
+------+----------+---+
|  John|       MIT| 26|
|Howard|       MIT| 59|
+------+----------+---+
```

see you next time with Apache Spark and Hortonworks platform

regards

Tomas