// lets prepare case class Student
case class Student(name:String,university:String,age:Int)

// lets create RDD from students.txt file
val rddFile = sc.textFile("/spark/students.txt")

// lets parse the input file line by line and get RDD [[lineArray1 words],[lineArray1 words]..]
val linesRDD = rddFile.map(line=>line.split(","))

// now lets create DataFrame by applying the case class Student onto every [lineX words array]
val studentsDF = linesRDD.map(lineArray=>Student(lineArray(0),lineArray(1),lineArray(2).toInt)).toDF()

// now register the students table with the students dataframe. In the last version of Spark use createOrReplaceTempView
studentsDF.registerTempTable("students")

// now lets select every MIT student in the Big Data storage:
val mitStudents = sqlContext.sql("SELECT name, university, age FROM students WHERE university = 'MIT'")

// show it
mitStudents.show()

// exit the shell
System.exit(0)
